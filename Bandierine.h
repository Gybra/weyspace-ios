//
//  Bandierine.h
//  WeySpace
//
//  Created by Luigi Leonardi on 25/02/13.
//  Copyright (c) 2013 Luigi Leonardi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Bandierine : UIViewController <UITableViewDataSource,UITableViewDelegate,UIScrollViewDelegate,UISearchBarDelegate,UISearchDisplayDelegate>{

    NSMutableArray *filteredListContent;

}
@property (strong, nonatomic) IBOutlet UITableView *tabella;
@property (nonatomic,retain) NSMutableArray *filteredListContent;

@end
