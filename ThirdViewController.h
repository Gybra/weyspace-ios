//
//  ThirdViewController.h
//  WeySpace
//
//  Created by Luigi Leonardi on 16/02/13.
//  Copyright (c) 2013 Luigi Leonardi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "InAppPacket.h"

@interface ThirdViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIImageView *img_2;
@property (strong, nonatomic) IBOutlet UIImageView *img_1;


@end
