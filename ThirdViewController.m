//
//  ThirdViewController.m
//  WeySpace
//
//  Created by Luigi Leonardi on 16/02/13.
//  Copyright (c) 2013 Luigi Leonardi. All rights reserved.
//

#import "ThirdViewController.h"
#import "Login.h"
#import "RegViewController.h"
#import "SBJson.h"
#import "MKStoreManager.h"
#import "AFHTTPClient.h"

@interface ThirdViewController ()

@end

@implementation ThirdViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = NSLocalizedString(@"Weyspace", @"Weyspace");
        self.tabBarItem.image = [UIImage imageNamed:@"third"];
    }
    return self;
}

- (void)viewDidLoad
{
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:[NSString stringWithFormat:@"%@: %@",NSLocalizedString(@"crediti", nil),[[NSUserDefaults standardUserDefaults]valueForKey:@"credit"]] style:UIBarButtonItemStylePlain target:self action:@selector(compra)];;

   // self.img_1.image = [UIImage imageNamed:@"image@2x.png"];
   // self.img_2.image = [UIImage imageNamed:@"640x912_immagine.png"];
    
    [NSTimer scheduledTimerWithTimeInterval:4.0
                                     target:self
                                   selector:@selector(scambia)
                                   userInfo:nil
                                    repeats:YES];
    
    [super viewDidLoad];
    
}

-(void)viewDidAppear:(BOOL)animated{
    [self aggiorna];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


-(void)compra{
    InAppPacket *pacchet;
    if([[UIDevice currentDevice]userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
        pacchet = [[InAppPacket alloc]initWithNibName:@"InAppPacket" bundle:nil];
    else
        pacchet = [[InAppPacket alloc]initWithNibName:@"InAppPacket_iPad" bundle:nil];
    
    [self presentViewController:pacchet animated:YES completion:nil];
}

-(void)aggiorna{
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:@"http://95.110.196.250/weyspace/json.php"]];
    NSDictionary *dictionary = [NSDictionary dictionaryWithObjectsAndKeys:@"yes",@"used",[[NSUserDefaults standardUserDefaults]valueForKey:@"email"],@"email", nil];
    
    [httpClient postPath:@"http://95.110.196.250/weyspace/json.php" parameters:dictionary success:^(AFHTTPRequestOperation *operation, id responseObject){
        
        NSData *resp = (NSData *)responseObject;
        
        SBJsonParser *pars = [[SBJsonParser alloc]init];
        NSArray *rip = [pars objectWithData:resp];
        
        self.navigationItem.rightBarButtonItem.title = [NSString stringWithFormat:@"%@: %i",NSLocalizedString(@"crediti", nil),[[[rip objectAtIndex:0]objectForKey:@"Credits"]intValue]];
        [[NSUserDefaults standardUserDefaults]setValue:[[rip objectAtIndex:0]objectForKey:@"Credits"] forKey:@"credit"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
    }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"%@   error",error);
    }];
    
}

-(void)scambia{
    UIImage *img;
    img = self.img_1.image;
    self.img_1.image = self.img_2.image;
    self.img_2.image = img;
}
@end
