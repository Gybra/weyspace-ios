//
//  Bandierine.m
//  WeySpace
//
//  Created by Luigi Leonardi on 25/02/13.
//  Copyright (c) 2013 Luigi Leonardi. All rights reserved.
//

#import "Bandierine.h"
#import "AFHTTPClient.h"
#import "SBJson.h"

@interface Bandierine () 

@end

@implementation Bandierine
{

    NSMutableArray *stati;

}
@synthesize filteredListContent;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    if (stati == NULL)
        stati = [[NSMutableArray alloc]init];
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(dismissViewControllerAnimated:completion:)];
    self.tabella.backgroundColor = [UIColor clearColor];
    self.tabella.separatorColor = [UIColor clearColor];
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:@"http://95.110.196.250/weyspace/json.php"]];
    NSDictionary *dictionary = [NSDictionary dictionaryWithObjectsAndKeys:@"yes",@"getnation", nil];
    
    [httpClient postPath:@"http://95.110.196.250/weyspace/json.php" parameters:dictionary success:^(AFHTTPRequestOperation *operation, id responseObject){
        
        NSData *resp = (NSData *)responseObject;
        
        SBJsonParser *pars = [[SBJsonParser alloc]init];
        stati = [pars objectWithData:resp];
        
        self.filteredListContent = [[NSMutableArray alloc]initWithCapacity:[stati count]];
        [self.tabella reloadData];
    }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"%@   error",error);
    }];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 1;
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(tableView == self.searchDisplayController.searchResultsTableView)
        return self.filteredListContent.count;
    else
        return stati.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *cellindentifier = @"cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellindentifier];
    if(cell==nil)
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellindentifier];
   
    if(tableView == self.searchDisplayController.searchResultsTableView)
        cell.textLabel.text = [self.filteredListContent objectAtIndex:indexPath.row];
    else
        cell.textLabel.text = [[stati objectAtIndex:indexPath.row]objectForKey:@"Nation"];
    
    [cell.textLabel setTextColor:[UIColor whiteColor]];

    cell.imageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@.png",cell.textLabel.text]];
    return cell;
    
    
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if(tableView == self.searchDisplayController.searchResultsTableView)
        [[NSUserDefaults standardUserDefaults]setValue:[self.filteredListContent objectAtIndex:indexPath.row] forKey:@"state"];
    else
        [[NSUserDefaults standardUserDefaults]setValue:[[stati objectAtIndex:indexPath.row]objectForKey:@"Nation"] forKey:@"state"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString{

    [self filterContentForSearchText:searchString scope:nil];
    return YES;

}

-(void)filterContentForSearchText:(NSString *)searchText scope:(NSString *)scope{

    [self.filteredListContent removeAllObjects];
    for(int i=0;i<stati.count;i++){
        NSComparisonResult result = [[[stati objectAtIndex:i]objectForKey:@"Nation"] compare:searchText options:(NSCaseInsensitiveSearch|NSDiacriticInsensitiveSearch) range:NSMakeRange(0, [searchText length]) ];
    
        if(result == NSOrderedSame)
            [self.filteredListContent addObject:[[stati objectAtIndex:i]objectForKey:@"Nation"]];
    }
}

-(void)searchDisplayController:(UISearchDisplayController *)controller willShowSearchResultsTableView:(UITableView *)tableView{

    [tableView setBackgroundColor:[UIColor clearColor]];
    UIView *vista = [[UIView alloc]initWithFrame:self.tabella.frame];
    UIImageView *img =[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"sfondo.png"]];
    
    CGRect rect = vista.frame;
    rect.origin.y -=44;
    img.frame = rect;
    [vista addSubview:img];
    
    [tableView setBackgroundView:vista]; 
    [tableView setSeparatorColor:[UIColor clearColor]];


}

-(void)searchDisplayController:(UISearchDisplayController *)controller willUnloadSearchResultsTableView:(UITableView *)tableView{

    tableView.backgroundView = nil;

}
@end
