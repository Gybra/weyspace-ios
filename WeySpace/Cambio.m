//
//  Cambio.m
//  WeySpace
//
//  Created by Luigi Leonardi on 28/02/13.
//  Copyright (c) 2013 Luigi Leonardi. All rights reserved.
//

#import "Cambio.h"
#import "AFHTTPClient.h"

@interface Cambio ()

@end

@implementation Cambio

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:NSLocalizedString(@"fatto", nil) style:UIBarButtonItemStyleDone target:self action:@selector(chiudi)];
    
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 200, 40)];
    label.text = NSLocalizedString(@"cambia_pass", nil);
    label.center = self.bottoncino.center;
    label.backgroundColor = [UIColor clearColor];
    label.textColor = [UIColor whiteColor];
    label.font = [UIFont fontWithName:@"Helvetica-Bold" size:20];
    label.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:label];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)cambia:(id)sender {
    
    if(![self.email.text isEqualToString:self.password.text])
    {[[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"errore", nil) message:NSLocalizedString(@"errore_pass", nil) delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil]show];
        return;
    }
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:@"http://95.110.196.250/weyspace/change_password.php"]];
    NSDictionary *dictionary = [NSDictionary dictionaryWithObjectsAndKeys:self.password.text,@"password",[[NSUserDefaults standardUserDefaults]valueForKey:@"email"],@"email", nil];
    
    [httpClient postPath:@"http://95.110.196.250/weyspace/change_password.php" parameters:dictionary success:^(AFHTTPRequestOperation *operation, id responseObject){
        
    }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"%@   error",error);
    }];

}

-(void)chiudi{
    [self dismissViewControllerAnimated:YES completion:nil];

}


-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}

@end
