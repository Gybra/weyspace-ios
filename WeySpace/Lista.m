//
//  Lista.m
//  WeySpace
//
//  Created by Luigi Leonardi on 17/02/13.
//  Copyright (c) 2013 Luigi Leonardi. All rights reserved.
//

#import "Lista.h"
#import "MHLazyTableImages.h"
#import "Vota.h"

@interface Lista () <MHLazyTableImagesDelegate>

@end

@implementation Lista{
	MHLazyTableImages *_lazyImages;
}
@synthesize dati,famoso;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        _lazyImages = [[MHLazyTableImages alloc] init];
        _lazyImages.delegate = self;
        _lazyImages.placeholderImage = [UIImage imageNamed:@"Placeholder"];
        self.title = NSLocalizedString(@"lista", nil);
    
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
	_lazyImages.tableView = self.tabella;
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(chiudi:)];
}

- (void)didReceiveMemoryWarning{

    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if(dati == NULL)
        return 1;
    else
        return 2;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{

    if(section == 0)
        return NSLocalizedString(@"vip", nil);
    else 
        return NSLocalizedString(@"non_vip", nil);

}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0)
       if(famoso)
        return 1;
    else
        return 0;
    else
        return [dati count];
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"LazyTableCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    if(indexPath.section == 0)
        cell.textLabel.text = [famoso objectForKey:@"name"];
else   
        cell.textLabel.text = [[dati objectAtIndex:indexPath.row]objectForKey:@"name"];
    
    [_lazyImages addLazyImageForCell:cell withIndexPath:indexPath];
 
    return cell;
}

- (IBAction)chiudi:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    indice = indexPath;
    Vota *view;
    if([[UIDevice currentDevice]userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
        view = [[Vota alloc]initWithNibName:@"Vota" bundle:nil];
    else
        view = [[Vota alloc]initWithNibName:@"Vota_iPad" bundle:nil];
    
    view.nome_testo =  [self.tabella cellForRowAtIndexPath:indexPath].textLabel.text;

    if(indexPath.section == 0)
        view.id_utente = [famoso objectForKey:@"id"];
    else
        view.id_utente = [[dati objectAtIndex:indexPath.row]objectForKey:@"id"];

    [self.navigationController pushViewController:view animated:YES];
    
}

- (NSURL *)lazyTableImages:(MHLazyTableImages *)lazyTableImages lazyImageURLForIndexPath:(NSIndexPath *)indexPath
{
    NSString *chi;
if(indexPath.section ==0)
    chi = [famoso objectForKey:@"id"];
else
    chi = [[dati objectAtIndex:indexPath.row]objectForKey:@"id"];

    
	return [NSURL URLWithString:[NSString stringWithFormat:@"https://graph.facebook.com/%@/picture",chi]];
}


- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
	[_lazyImages scrollViewDidEndDragging:scrollView willDecelerate:decelerate];
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
	[_lazyImages scrollViewDidEndDecelerating:scrollView];
}

-(void)viewDidAppear:(BOOL)animated{

if ((self.famoso.count == 0)&&(self.dati.count == 0))
    [[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"attenzione", nil) message:[NSString stringWithFormat:@"%@/n%@",NSLocalizedString(@"nessuno_1", nil),NSLocalizedString(@"nessuno_2", nil)] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil]show];

}

@end
