//
//  RegViewController.m
//  WeySpace
//
//  Created by Luigi Leonardi on 16/02/13.
//  Copyright (c) 2013 Luigi Leonardi. All rights reserved.
//

#import "RegViewController.h"
#import "SBJson.h"
#import "AFHTTPClient.h"
#import "Regolamento.h"

@interface RegViewController () {
    UIPopoverController *pop2;
}

@end

@implementation RegViewController
@synthesize name,surname,username,password,email,birth,gender,stato,scrollone,pick,to;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    scrollone.contentSize = CGSizeMake(320, 580);
    [self.bottone_switch setBackgroundImage:[UIImage imageNamed:@"placeholder.png"] forState:UIControlStateNormal];
    
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 200, 40)];
    label.text = NSLocalizedString(@"crea", nil);
    label.center = self.bottoncino.center;
    label.backgroundColor = [UIColor clearColor];
    label.textColor = [UIColor whiteColor];
    label.font = [UIFont fontWithName:@"Helvetica-Bold" size:20];
    label.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:label];
    // Do any additional setup after loading the view from its nib.
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)crea:(id)sender {
    
    if([self.bottone_switch currentBackgroundImage] == [UIImage imageNamed:@"placeholder.png"])
    {
        [[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"errore", nil) message:NSLocalizedString(@"termini", nil) delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil]show];
        return;
    }
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:@"http://95.110.196.250/weyspace/put_a_user.php"]];
    NSDictionary *dictionary = [NSDictionary dictionaryWithObjectsAndKeys:self.state.text,@"nazione",self.password.text,@"password",self.birth.text,@"birth",self.gender.text,@"gender",self.email.text,@"email",self.name.text,@"name",self.surname.text,@"surname", nil];
    
    [httpClient postPath:@"http://95.110.196.250/weyspace/put_a_user.php" parameters:dictionary success:^(AFHTTPRequestOperation *operation, id responseObject){
        
        NSString *resp = (NSString *)responseObject;
        
        if([resp length]==0) {
            [[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"attenzione", nil) message:NSLocalizedString(@"alert", nil) delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil]show];
            [[NSUserDefaults standardUserDefaults] setValue:self.email.text forKey:@"email"];
            [[NSUserDefaults standardUserDefaults] setValue:self.state.text forKey:@"state"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            [self dismissViewControllerAnimated:YES completion:nil];
        }
       
    }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"attenzione", nil) message:NSLocalizedString(@"utente_esiste", nil) delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil]show];
        
        NSLog(@"%@   error",error);
    }];
    
    

    
}

- (IBAction)apri:(id)sender {
    Regolamento *reg;
    if([[UIDevice currentDevice]userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
        reg = [[Regolamento alloc]initWithNibName:@"Regolamento" bundle:nil];
    else
        reg = [[Regolamento alloc]initWithNibName:@"Regolamento_iPad" bundle:nil];
    
    [self presentViewController:reg animated:YES completion:nil];
}


-(NSDictionary *)parseJsonFromUrl:(NSURL *)url
{
    SBJsonParser *parser = [[SBJsonParser alloc] init];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    NSData *response = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
    NSString *json_string = [[NSString alloc] initWithData:response encoding:NSUTF8StringEncoding];
    return [parser objectWithString:json_string];
}

- (IBAction)foto:(id)sender {
    UIImagePickerController *con = [[UIImagePickerController alloc]init];
    con = [[UIImagePickerController alloc] init];
    con.delegate = self;
    con.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    con.mediaTypes = [NSArray arrayWithObject:(NSString *)kUTTypeImage];
    con.allowsEditing = YES;
    
    if([[UIDevice currentDevice]userInterfaceIdiom]==UIUserInterfaceIdiomPhone)
        [self presentViewController:con animated:YES completion:nil];
    else{
        
        UIButton *botti = (UIButton *)sender;
        pop2 = [[UIPopoverController alloc]initWithContentViewController:con];
        [pop2 setPopoverContentSize:CGSizeMake(320, 400) animated:YES];
        
        [pop2 presentPopoverFromRect:botti.bounds inView:botti permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
        [pop2 setDelegate:self];
        
    }
}

- (IBAction)cambia {

    if([self.bottone_switch currentBackgroundImage] == [UIImage imageNamed:@"placeholder.png"])
        [self.bottone_switch setBackgroundImage:[UIImage imageNamed:@"check.png"] forState:UIControlStateNormal];
    else
        [self.bottone_switch setBackgroundImage:[UIImage imageNamed:@"placeholder.png"] forState:UIControlStateNormal];
}

- (IBAction)data {
    [self.view endEditing:YES];
    self.pick = [[UIDatePicker alloc]initWithFrame:CGRectMake(0, self.view.frame.size.height-216, 320, 216)];
    self.pick.datePickerMode = UIDatePickerModeDate;
    self.pick.maximumDate = [NSDate date];
    self.pick.locale = [[NSLocale alloc]initWithLocaleIdentifier:[[NSLocale preferredLanguages]objectAtIndex:0]];

    self.to = [[UIToolbar alloc]initWithFrame:CGRectMake(0, pick.frame.origin.y-44, self.view.frame.size.width, 44.0)];
    self.to.barStyle = UIBarStyleBlack;
    self.to.translucent = YES;
    UIBarButtonItem *done = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(scegli)];
    UIBarButtonItem *space = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    self.to.items = [[NSMutableArray alloc]initWithObjects:space,done, nil];
    
    [[self view]addSubview:self.to];
    [[self view]addSubview:self.pick];
}

- (void) imagePickerController:(UIImagePickerController *)picker
 didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *imagePath = [documentsDirectory stringByAppendingPathComponent:@"profilo.png"];
    
    UIImage *editedImage = [info objectForKey:UIImagePickerControllerEditedImage];
    
    NSData *webData = UIImageJPEGRepresentation(editedImage, 0.3);
    [webData writeToFile:imagePath atomically:YES];
    [[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"successo", nil) message:NSLocalizedString(@"img_salvata", nil) delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil]show];
    [picker dismissViewControllerAnimated:YES completion:nil];
}


- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self animateTextField:textField up:YES];
}


- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [self animateTextField:textField up:NO];
}

- (void) animateTextField: (UITextField*) textField up: (BOOL) up
{
    int animatedDistance;
    int moveUpValue = textField.frame.origin.y+ textField.frame.size.height;
    UIInterfaceOrientation orientation =
    [[UIApplication sharedApplication] statusBarOrientation];
    if (orientation == UIInterfaceOrientationPortrait ||
        orientation == UIInterfaceOrientationPortraitUpsideDown)
    {
        
        animatedDistance = 250-(460-moveUpValue-5);
    }
    else
    {
        animatedDistance = 160-(320-moveUpValue-5);
    }
    
    if(animatedDistance>0)
    {
        const int movementDistance = animatedDistance;
        const float movementDuration = 0.3f;
        int movement = (up ? -movementDistance : movementDistance);
        [UIView beginAnimations: nil context: nil];
        [UIView setAnimationBeginsFromCurrentState: YES];
        [UIView setAnimationDuration: movementDuration];
        self.view.frame = CGRectOffset(self.view.frame, 0, movement);
        [UIView commitAnimations];
    }
}

-(void)scegli{
    NSDateFormatter *form = [[NSDateFormatter alloc]init];
    [form setDateFormat:@"dd/MM/yyyy"];
    self.birth.text = [NSString stringWithFormat:@"%@",[form stringFromDate:[self.pick date]]];
    [self.pick removeFromSuperview];
    [self.to removeFromSuperview];

}

@end
