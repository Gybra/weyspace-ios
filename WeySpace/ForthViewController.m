//
//  ForthViewController.m
//  WeySpace
//
//  Created by Luigi Leonardi on 27/02/13.
//  Copyright (c) 2013 Luigi Leonardi. All rights reserved.
//

#import "ForthViewController.h"
#import "AFHTTPClient.h"
#import "Cambio.h"
#import "SBJson.h"

@interface ForthViewController (){

    UIPopoverController *pop,*pop2;

}

@end

@implementation ForthViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.tabBarItem.image = [UIImage imageNamed:@"settings.png"];
        self.title = NSLocalizedString(@"impostazioni", nil);
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:[NSString stringWithFormat:@"%@: %@",NSLocalizedString(@"crediti", nil),[[NSUserDefaults standardUserDefaults]valueForKey:@"credit"]] style:UIBarButtonItemStylePlain target:self action:@selector(compra)];;

    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 200, 40)];
    label.text = NSLocalizedString(@"cambia_pass", nil);
    label.center = self.bottoncino.center;
    label.backgroundColor = [UIColor clearColor];
    label.textColor = [UIColor whiteColor];
    label.font = [UIFont fontWithName:@"Helvetica-Bold" size:20];
    label.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:label];
    
    UILabel *label2 = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 200, 40)];
    label2.text = NSLocalizedString(@"manda_email", nil);
    label2.center = self.bottoncino2.center;
    label2.backgroundColor = [UIColor clearColor];
    label2.textColor = [UIColor whiteColor];
    label2.font = [UIFont fontWithName:@"Helvetica-Bold" size:20];
    label2.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:label2];
    
    UILabel *label3 = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 250, 40)];
    label3.text = NSLocalizedString(@"agg_foto", nil);
    label3.center = self.bottoncino3.center;
    label3.backgroundColor = [UIColor clearColor];
    label3.textColor = [UIColor whiteColor];
    label3.font = [UIFont fontWithName:@"Helvetica-Bold" size:20];
    label3.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:label3];
}

- (void)viewDidAppear:(BOOL)animated
{
    [self aggiorna];    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (IBAction)change:(id)sender {
 
    Cambio *ca = [[Cambio alloc]initWithNibName:@"Cambio" bundle:nil];
    UINavigationController *contr = [[UINavigationController alloc]initWithRootViewController:ca];
        contr.navigationBar.tintColor = [UIColor blackColor];
if([[UIDevice currentDevice]userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
     [self presentViewController:contr animated:YES completion:nil];
else{
    UIButton *bot = (UIButton *)sender;
    pop = [[UIPopoverController alloc]initWithContentViewController:contr];
    [pop setPopoverContentSize:CGSizeMake(ca.view.frame.size.width, ca.view.frame.size.height+ca.navigationController.navigationBar.frame.size.height) animated:YES];
    [pop presentPopoverFromRect:bot.bounds inView:bot permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    [pop setDelegate:self];
}

}

- (IBAction)send:(id)sender {
    
    MFMailComposeViewController *controller=[[MFMailComposeViewController alloc]init];
    [controller setSubject:@"Info"];
    [controller setToRecipients:[NSArray arrayWithObject:[NSString stringWithFormat:@"help@weyspace.com"]]];
    [controller setSubject:[NSString stringWithFormat:@"Help from %@",[[NSUserDefaults standardUserDefaults]valueForKey:@"email"]]];
    controller.mailComposeDelegate=self;
    controller.navigationBar.tintColor = [UIColor blackColor];
       
    [self presentViewController:controller animated:YES completion:nil];


}

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)aggiorna{
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:@"http://95.110.196.250/weyspace/json.php"]];
    NSDictionary *dictionary = [NSDictionary dictionaryWithObjectsAndKeys:@"yes",@"used",[[NSUserDefaults standardUserDefaults]valueForKey:@"email"],@"email", nil];
    
    [httpClient postPath:@"http://95.110.196.250/weyspace/json.php" parameters:dictionary success:^(AFHTTPRequestOperation *operation, id responseObject){
        
        NSData *resp = (NSData *)responseObject;
        
        SBJsonParser *pars = [[SBJsonParser alloc]init];
        NSArray *rip = [pars objectWithData:resp];
        
        self.navigationItem.rightBarButtonItem.title = [NSString stringWithFormat:@"%@: %i",NSLocalizedString(@"crediti", nil),[[[rip objectAtIndex:0]objectForKey:@"Credits"]intValue]];
        
        [[NSUserDefaults standardUserDefaults]setValue:[[rip objectAtIndex:0]objectForKey:@"Credits"] forKey:@"credit"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
    }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"%@   error",error);
    }];
    
}


-(void)compra{
    InAppPacket *pacchet;
    if([[UIDevice currentDevice]userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
        pacchet = [[InAppPacket alloc]initWithNibName:@"InAppPacket" bundle:nil];
    else
        pacchet = [[InAppPacket alloc]initWithNibName:@"InAppPacket_iPad" bundle:nil];
    
    [self presentViewController:pacchet animated:YES completion:nil];
}

-(IBAction)foto:(id)sender{
    UIImagePickerController *con = [[UIImagePickerController alloc]init];
    con = [[UIImagePickerController alloc] init];
    con.delegate = self;
    con.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    con.mediaTypes = [NSArray arrayWithObject:(NSString *)kUTTypeImage];
    con.allowsEditing = YES;

if([[UIDevice currentDevice]userInterfaceIdiom]==UIUserInterfaceIdiomPhone)
    [self presentViewController:con animated:YES completion:nil];
else{
    
    UIButton *botti = (UIButton *)sender;
    pop2 = [[UIPopoverController alloc]initWithContentViewController:con];
    [pop2 setPopoverContentSize:CGSizeMake(320, 400) animated:YES];
    
    [pop2 presentPopoverFromRect:botti.bounds inView:botti permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    [pop2 setDelegate:self];

}
}

- (void) imagePickerController:(UIImagePickerController *)picker
 didFinishPickingMediaWithInfo:(NSDictionary *)info {

    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *imagePath = [documentsDirectory stringByAppendingPathComponent:@"profilo.png"];
    
    UIImage *editedImage = [info objectForKey:UIImagePickerControllerEditedImage];
  
    NSData *webData = UIImageJPEGRepresentation(editedImage, 0.5);
    [webData writeToFile:imagePath atomically:YES];
    [[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"successo", nil) message:NSLocalizedString(@"img_salvata", nil) delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil]show];

    
    [picker dismissViewControllerAnimated:YES completion:nil];
}

-(BOOL)popoverControllerShouldDismissPopover:(UIPopoverController *)popoverController{
    return YES;
}
@end
