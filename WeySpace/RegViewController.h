//
//  RegViewController.h
//  WeySpace
//
//  Created by Luigi Leonardi on 16/02/13.
//  Copyright (c) 2013 Luigi Leonardi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MobileCoreServices/MobileCoreServices.h"

@interface RegViewController : UIViewController <UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIPopoverControllerDelegate> {

    NSString *stato;
    UIDatePicker *pick;
    UIToolbar *to;

}

@property (nonatomic,retain) UIDatePicker *pick;
@property (nonatomic,retain) UIToolbar *to;

@property (strong, nonatomic) IBOutlet UIScrollView *scrollone;
@property (weak, nonatomic) IBOutlet UIButton *bottone_switch;
@property (strong, nonatomic) IBOutlet UITextField *name;
@property (strong, nonatomic) IBOutlet UITextField *surname;
@property (strong, nonatomic) IBOutlet UITextField *username;
@property (strong, nonatomic) IBOutlet UITextField *password;
@property (strong, nonatomic) IBOutlet UITextField *email;
@property (strong, nonatomic) IBOutlet UITextField *birth;
@property (strong, nonatomic) IBOutlet UITextField *gender;
@property (strong, nonatomic) NSString *stato;
- (IBAction)crea:(id)sender;
- (IBAction)apri:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *state;
- (IBAction)foto:(id)sender;
- (IBAction)cambia;
- (IBAction)data;
@property (strong, nonatomic) IBOutlet UIButton *bottoncino;

@end
