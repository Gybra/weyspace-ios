//  FirstViewController.m
//  WeySpace
//
//  Created by Luigi Leonardi on 13/02/13.
//  Copyright (c) 2013 Luigi Leonardi. All rights reserved.
//

#import "FirstViewController.h"
#import "SBJson.h"
#import "MHLazyTableImages.h"
#import "AFHTTPClient.h"
#import "Bandierine.h"
#import "Vota.h"
#import "RegViewController.h"
#import "Login.h"
#import "InAppPacket.h"

@interface FirstViewController () <MHLazyTableImagesDelegate>

@end

@implementation FirstViewController {

    MHLazyTableImages *_lazyImages;
    MHLazyTableImages *_lazyImages_2;
    MHLazyTableImages *_lazyImages_3;

    NSMutableArray *tabelle,*stati;
    UIAlertView *alert3,*alert4;
    UINavigationController *nav;
    int somma;
    
}
@synthesize scroll,page_control,dict,dict_2,state,index,stato,dict3;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
        self.tabBarItem.title = NSLocalizedString(@"classifiche", nil);
        self.navigationItem.title = NSLocalizedString(@"mondiali", nil);
        self.
        self.tabBarItem.image = [UIImage imageNamed:@"first.png"];
     
        _lazyImages = [[MHLazyTableImages alloc] init];
        _lazyImages.delegate = self;
        _lazyImages.placeholderImage = [UIImage imageNamed:@"Placeholder"];
        
        _lazyImages_2 = [[MHLazyTableImages alloc] init];
        _lazyImages_2.delegate = self;
        _lazyImages_2.placeholderImage = [UIImage imageNamed:@"Placeholder"];
        
        _lazyImages_3 = [[MHLazyTableImages alloc] init];
        _lazyImages_3.delegate = self;
        _lazyImages_3.placeholderImage = [UIImage imageNamed:@"Placeholder"];
        
        self.view.backgroundColor = [UIColor lightGrayColor];
        self.scroll.backgroundColor = [UIColor clearColor];

    }
    return self;
}

- (void)viewDidLoad
{
    
    [self.tabBarController.tabBar setHidden:NO];
    [self.navigationController.navigationBar setHidden:NO];
    
    [super viewDidLoad];
    
    [NSTimer scheduledTimerWithTimeInterval:5.0
                                     target:self
                                   selector:@selector(scrambla)
                                   userInfo:nil
                                    repeats:YES];
     self.state = [[NSUserDefaults standardUserDefaults]valueForKey:@"state"];
    if(self.state == NULL)
        self.state = @"Italy";
    
    tabelle=[[NSMutableArray alloc]initWithCapacity:2];
    dict_2 = [self parseJsonFromUrl:[NSURL URLWithString:@"http://95.110.196.250/weyspace/json.php"]];
            
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:@"http://95.110.196.250/weyspace/json.php"]];
    NSDictionary *dictionary = [NSDictionary dictionaryWithObjectsAndKeys:self.state,@"nazione", nil];
    [httpClient postPath:@"http://95.110.196.250/weyspace/json.php" parameters:dictionary success:^(AFHTTPRequestOperation *operation, id responseObject){
        
        NSData *resp = (NSData *)responseObject;
        
        SBJsonParser *pars = [[SBJsonParser alloc]init];
       dict = [pars objectWithData:resp];
        [self crea];
        
        dict3 = dict;
        
    }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"%@   error",error);
    }];  
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@.png",self.state]] style:UIBarButtonItemStylePlain target:self action:@selector(cambia)];
   
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:[NSString stringWithFormat:@"%@: %@",NSLocalizedString(@"credit", nil),[[NSUserDefaults standardUserDefaults]valueForKey:@"credit"]] style:UIBarButtonItemStylePlain target:self action:@selector(compra)];

    
}

-(void)cambia{
    Bandierine *band = [[Bandierine alloc]initWithNibName:@"Bandierine" bundle:nil];
    [self presentViewController:band animated:YES completion:nil];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)viewDidAppear:(BOOL)animated{
    [self.tabBarController.tabBar setHidden:NO];

    self.state = [[NSUserDefaults standardUserDefaults]valueForKey:@"state"];
    if(self.state == NULL)
        self.state = @"Italy";
    
    if(self.page_control.currentPage == 1)
        self.navigationItem.title = self.state;
    
    self.navigationItem.leftBarButtonItem.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@.png",self.state]];

if([[NSUserDefaults standardUserDefaults]valueForKey:@"email"]==NULL){
        [self creavista];
}
    
    [self reload];
    [self aggiorna];
    
}


-(NSMutableArray *)parseJsonFromUrl:(NSURL *)url
{
    SBJsonParser *parser = [[SBJsonParser alloc] init];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    NSData *response = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
    NSString *json_string = [[NSString alloc] initWithData:response encoding:NSUTF8StringEncoding];
    return [parser objectWithString:json_string];
}


-(NSDictionary *)parse2JsonFromUrl:(NSURL *)url
{
    SBJsonParser *parser = [[SBJsonParser alloc] init];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    NSData *response = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
    NSString *json_string = [[NSString alloc] initWithData:response encoding:NSUTF8StringEncoding];
    return [parser objectWithString:json_string];
}

-(void)crea{
    
for (int i = 0; i < 3; i++) {
    
    CGRect frame;

    frame.origin.x = self.scroll.frame.size.width * i;
    frame.origin.y = 0;
    frame.size = self.scroll.frame.size;
    
    UITableView *tabella = [[UITableView alloc]initWithFrame:self.scroll.frame style:UITableViewStylePlain];
    tabella.delegate    = self;
    tabella.dataSource  = self;
    
    tabella.backgroundView = NULL;
    tabella.backgroundColor = [UIColor clearColor];
    
    tabella.separatorColor = [UIColor clearColor];

    tabella.center = CGPointMake(frame.origin.x + frame.size.width/2, frame.size.height/2);

    if(i==0)
        _lazyImages.tableView = tabella;
    else if(i==1)
        _lazyImages_2.tableView = tabella;
    else
        _lazyImages_3.tableView = tabella;
    
    [tabelle addObject:tabella];
    [self.scroll addSubview:[tabelle objectAtIndex:i]];
    
    }
    
    self.scroll.contentSize = CGSizeMake(self.scroll.frame.size.width *3, self.scroll.frame.size.height);
    self.page_control.currentPage = 0;
    self.page_control.numberOfPages = 3;

}

-(IBAction)changePage{
    CGRect frame;
    frame.origin.x = self.scroll.frame.size.width * self.page_control.currentPage;
    frame.origin.y = 0;
    frame.size = self.scroll.frame.size;
    [self.scroll scrollRectToVisible:frame animated:YES];
    pageControlBeingUsed = YES;
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    if(!pageControlBeingUsed){
        CGFloat pageWidth = self.scroll.frame.size.width;
        int page = floor((self.scroll.contentOffset.x - pageWidth /2)/pageWidth)+1;
        self.page_control.currentPage = page;
        
        if(page==0)
            self.navigationItem.title = NSLocalizedString(@"mondiali", nil);
        else if (page==1)
            self.navigationItem.title = self.state;
        else
            self.navigationItem.title = NSLocalizedString(@"casaccio", nil);
    }
    
}

-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    
    pageControlBeingUsed = NO;
    
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    
    pageControlBeingUsed = NO;
    [_lazyImages scrollViewDidEndDecelerating:scrollView];
    [_lazyImages_2 scrollViewDidEndDecelerating:scrollView];

    
}


#pragma mark UITableView

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{

    return 1;

}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(tableView == [tabelle objectAtIndex:0])
        return [dict_2 count];
    else if(tableView == [tabelle objectAtIndex:1])
        return [dict count];
    else
        return [dict3 count];

}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

    static NSString *cellindentifier = @"cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellindentifier];

    if(cell==nil)
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellindentifier];
    
    cell.backgroundColor = [UIColor clearColor];
    cell.backgroundView = nil;
    [cell.textLabel setTextColor:[UIColor whiteColor]];

    UIProgressView *progress = [[UIProgressView alloc] initWithProgressViewStyle:UIProgressViewStyleBar];

    if(tableView == [tabelle objectAtIndex:0]){
        cell.textLabel.text = [NSString stringWithFormat:@"%@",[[self.dict_2 objectAtIndex:indexPath.row]objectForKey:@"Name"]];

        [_lazyImages addLazyImageForCell:cell withIndexPath:indexPath];
        progress.progress = [self percentuale:indexPath.row chi:1];

        
    } else if(tableView == [tabelle objectAtIndex:1]) {
        cell.textLabel.text = [NSString stringWithFormat:@"%@",[[self.dict objectAtIndex:indexPath.row]objectForKey:@"Name"]];
        [_lazyImages_2 addLazyImageForCell:cell withIndexPath:indexPath];
        progress.progress = [self percentuale:indexPath.row chi:0];

    } else{
        cell.textLabel.text = [NSString stringWithFormat:@"%@",[[self.dict3 objectAtIndex:indexPath.row]objectForKey:@"Name"]];
        [_lazyImages_3 addLazyImageForCell:cell withIndexPath:indexPath];
        return cell;
    }
    
    CGRect re = cell.imageView.frame;
    re.size.width = cell.imageView.frame.size.height;
    cell.imageView.contentMode =  UIViewContentModeScaleAspectFit;

    
    [cell setFrame:re];

    progress.frame = CGRectMake(10, 10, 250, 30);
    progress.tag = 123;
    progress.center = CGPointMake(177,37);
        
    [cell.contentView addSubview:progress];
        
    return cell;
    

}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    Vota *vota;
    if([[UIDevice currentDevice]userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
        vota = [[Vota alloc]initWithNibName:@"Vota" bundle:nil];
    else
        vota = [[Vota alloc]initWithNibName:@"Vota_iPad" bundle:nil];
    
    if(tableView ==[tabelle objectAtIndex:0]){
        vota.id_utente = [[self.dict_2 objectAtIndex:indexPath.row]objectForKey:@"UDID"];
        vota.nome_testo = [[self.dict_2 objectAtIndex:indexPath.row]objectForKey:@"Name"];
        vota.url = [[self.dict_2 objectAtIndex:indexPath.row]objectForKey:@"Picture"];
    } else if(tableView==[tabelle objectAtIndex:2]){
        vota.id_utente = [[self.dict3 objectAtIndex:indexPath.row]objectForKey:@"UDID"];
        vota.nome_testo = [[self.dict3 objectAtIndex:indexPath.row]objectForKey:@"Name"];
        vota.url = [[self.dict3 objectAtIndex:indexPath.row]objectForKey:@"Picture"];
    }else {
        vota.id_utente = [[self.dict objectAtIndex:indexPath.row]objectForKey:@"UDID"];
        vota.nome_testo = [[self.dict objectAtIndex:indexPath.row]objectForKey:@"Name"];
        vota.url = [[self.dict objectAtIndex:indexPath.row]objectForKey:@"Picture"];
    }
    vota.hidesBottomBarWhenPushed = YES;

    
    [self.navigationController pushViewController:vota animated:YES];
    
}

- (NSURL *)lazyTableImages:(MHLazyTableImages *)lazyTableImages lazyImageURLForIndexPath:(NSIndexPath *)indexPath
{
    NSString *chi;
    if(lazyTableImages == _lazyImages)
        chi = [[dict_2 objectAtIndex:indexPath.row]objectForKey:@"Picture"];
    else if(lazyTableImages == _lazyImages_2)
        chi = [[dict objectAtIndex:indexPath.row]objectForKey:@"Picture"];
    else
        chi = [[dict3 objectAtIndex:indexPath.row]objectForKey:@"Picture"];
    
	return [NSURL URLWithString:chi];
}


- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
	[_lazyImages scrollViewDidEndDragging:scrollView willDecelerate:decelerate];
    [_lazyImages_2 scrollViewDidEndDragging:scrollView willDecelerate:decelerate];

}

-(void)compra{
    InAppPacket *pacchet;
 if([[UIDevice currentDevice]userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
    pacchet = [[InAppPacket alloc]initWithNibName:@"InAppPacket" bundle:nil];
else
    pacchet = [[InAppPacket alloc]initWithNibName:@"InAppPacket_iPad" bundle:nil];

    [self presentViewController:pacchet animated:YES completion:nil];
}

-(void)incrementa{
   
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:@"http://95.110.196.250/weyspace/json.php"]];
    NSDictionary *dictionary = [NSDictionary dictionaryWithObjectsAndKeys:@"yes",@"credits", nil];
    
    [httpClient postPath:@"http://95.110.196.250/weyspace/json.php" parameters:dictionary success:^(AFHTTPRequestOperation *operation, id responseObject){
        
        NSData *resp = (NSData *)responseObject;
        
        SBJsonParser *pars = [[SBJsonParser alloc]init];
        NSArray *rip = [pars objectWithData:resp];
        int ora = [[[NSUserDefaults standardUserDefaults]valueForKey:@"credit"]intValue];
        ora += [[[rip objectAtIndex:0]objectForKey:@"How"]intValue];
        [[NSUserDefaults standardUserDefaults]setValue:[NSString stringWithFormat:@"%i",ora] forKey:@"credit"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        [self aggiorna];
        
        
    }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"%@   error",error);
    }];
    
    

}

-(void)aggiorna{
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:@"http://95.110.196.250/weyspace/json.php"]];
    NSDictionary *dictionary = [NSDictionary dictionaryWithObjectsAndKeys:@"yes",@"used",[[NSUserDefaults standardUserDefaults]valueForKey:@"email"],@"email", nil];
    
    [httpClient postPath:@"http://95.110.196.250/weyspace/json.php" parameters:dictionary success:^(AFHTTPRequestOperation *operation, id responseObject){
        
        NSData *resp = (NSData *)responseObject;
        
        SBJsonParser *pars = [[SBJsonParser alloc]init];
        NSArray *rip = [pars objectWithData:resp];

        self.navigationItem.rightBarButtonItem.title = [NSString stringWithFormat:@"%@: %i",NSLocalizedString(@"crediti", nil),[[[rip objectAtIndex:0]objectForKey:@"Credits"]intValue]];

        [[NSUserDefaults standardUserDefaults]setValue:[[rip objectAtIndex:0]objectForKey:@"Credits"] forKey:@"credit"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
        
    }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"%@   error",error);
    }];
    
}


-(void)reload{

    dict_2 = [self parseJsonFromUrl:[NSURL URLWithString:@"http://95.110.196.250/weyspace/json.php"]];
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:@"http://95.110.196.250/weyspace/json.php"]];
    NSDictionary *dictionary = [NSDictionary dictionaryWithObjectsAndKeys:self.state,@"nation", nil];
    [httpClient postPath:@"http://95.110.196.250/weyspace/json.php" parameters:dictionary success:^(AFHTTPRequestOperation *operation, id responseObject){
        
        NSData *resp = (NSData *)responseObject;
        NSString *resp_string = (NSString *)responseObject;

        dict = NULL;
        SBJsonParser *pars = [[SBJsonParser alloc]init];
        if([resp_string length]==4){
            dict = NULL;
            [[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"errore", nil) message:NSLocalizedString(@"no_voti", nil) delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil]show];
            [self cambia];
        
        }
    else
        dict = [pars objectWithData:resp];

        [[tabelle objectAtIndex:0]reloadData];
        [[tabelle objectAtIndex:1]reloadData];
        
    }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"%@   error",error);
    }];
}

-(void)crea_acco{
    RegViewController *reg;
    if([[UIDevice currentDevice]userInterfaceIdiom] == UIUserInterfaceIdiomPhone){

    if([[UIScreen mainScreen]bounds].size.height == 568)
        reg = [[RegViewController alloc]initWithNibName:@"RegViewController" bundle:nil];
    else
        reg = [[RegViewController alloc]initWithNibName:@"RegViewController_4" bundle:nil];
    }
    else
        reg = [[RegViewController alloc]initWithNibName:@"RegViewController_iPad" bundle:nil];
    [nav pushViewController:reg animated:YES];
}

-(void)login{
    Login *reg;
    if([[UIDevice currentDevice]userInterfaceIdiom] == UIUserInterfaceIdiomPhone){
    if([[UIScreen mainScreen]bounds].size.height == 568)
        reg = [[Login alloc]initWithNibName:@"Login" bundle:nil];
    else
        reg = [[Login alloc]initWithNibName:@"Login_4" bundle:nil];
    } else
        reg = [[Login alloc]initWithNibName:@"Login_iPad" bundle:nil];
   
    [nav pushViewController:reg animated:YES];
}

-(float)percentuale:(int)riga chi:(int)tab{
if(tab==0){
    float x = 0;
    int indice1 = 0;

    for(int i=0;i<dict.count;i++)
        indice1+=[[[dict objectAtIndex:i]objectForKey:@"Votes"]intValue];
    
    x = [[[dict objectAtIndex:riga]objectForKey:@"Votes"]intValue];
    x/= indice1;
    
    return x;
}
else {

    float x = 0;
    int indice1 = 0;
    
    for(int i=0;i<dict_2.count;i++)
        indice1+=[[[dict_2 objectAtIndex:i]objectForKey:@"Votes"]intValue];
    
    x = [[[dict_2 objectAtIndex:riga]objectForKey:@"Votes"]intValue];
    x/= indice1;
    
    return x;

}

}


-(void)creavista{
    
    UIView *vista_view = [[UIView alloc]initWithFrame:[[UIScreen mainScreen]bounds]];
    UIImageView *im = [[UIImageView alloc]initWithFrame:[vista_view frame]];
    im.image = [UIImage imageNamed:@"sfondo.png"];
    [vista_view addSubview:im];
    UIButton *bottone=[UIButton buttonWithType:UIButtonTypeRoundedRect];
    bottone.frame=CGRectMake(0, 0, 270, 51);
    bottone.center=CGPointMake(vista_view.frame.size.width/2, 100 +70 -self.navigationController.navigationBar.frame.size.height);
    [bottone setBackgroundImage:[UIImage imageNamed:@"vuoto.png"] forState:UIControlStateNormal];
    [bottone addTarget:self action:@selector(crea_acco) forControlEvents:UIControlEventTouchUpInside];
    
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 200, 40)];
    label.text = NSLocalizedString(@"crea", nil);
    label.center = bottone.center;
    label.backgroundColor = [UIColor clearColor];
    label.textColor = [UIColor whiteColor];
    label.font = [UIFont fontWithName:@"Helvetica-Bold" size:20];
    label.textAlignment = NSTextAlignmentCenter;
    
    
    
    UIButton *bottone2=[UIButton buttonWithType:UIButtonTypeRoundedRect];
    bottone2.frame=CGRectMake(0, 0, 270, 51);
    bottone2.center=CGPointMake(vista_view.frame.size.width/2, 50 +50 - self.navigationController.navigationBar.frame.size.height);
    [bottone2 setBackgroundImage:[UIImage imageNamed:@"vuoto.png"] forState:UIControlStateNormal];
    [bottone2 addTarget:self action:@selector(login) forControlEvents:UIControlEventTouchUpInside];
    
    UILabel *label2 = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 200, 40)];
    label2.text = NSLocalizedString(@"logga", nil);
    label2.center = bottone2.center;
    label2.backgroundColor = [UIColor clearColor];
    label2.textColor = [UIColor whiteColor];
    label2.font = [UIFont fontWithName:@"Helvetica-Bold" size:20];
    label2.textAlignment = NSTextAlignmentCenter;
    
    
    [vista_view addSubview:bottone];
    [vista_view addSubview:bottone2];
    [vista_view addSubview:label];
    [vista_view addSubview:label2];

    
    UILabel *giacomo = [[UILabel alloc]initWithFrame:CGRectMake(vista_view.frame.size.width/2, 450, 300, 30)];
    [giacomo setCenter:CGPointMake(vista_view.frame.size.width/2, 478)];
    [giacomo setTextAlignment:NSTextAlignmentCenter];
    [giacomo setTextColor:[UIColor whiteColor]];
    [giacomo setText:@"© Weyspace.com"];
    [giacomo setBackgroundColor:[UIColor clearColor]];
    
    [vista_view addSubview:giacomo];
    
    [[NSUserDefaults standardUserDefaults]setValue:[NSString stringWithFormat:@"0"] forKey:@"credit"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    UIViewController *vista = [[UIViewController alloc]init];
    vista.modalPresentationStyle = UIModalTransitionStyleCoverVertical;
    vista.view = vista_view;
    vista.title = @"Login";
    nav = [[UINavigationController alloc]initWithRootViewController:vista];
    nav.navigationBar.tintColor = [UIColor blackColor];
    [self presentViewController:nav animated:YES completion:nil];
}

-(void)scrambla{
    
    for (int i = [dict3 count]; i > 1; i--)
        [dict3 exchangeObjectAtIndex:i-1 withObjectAtIndex:random()%i];

    [[tabelle objectAtIndex:2]reloadData];
}
@end
