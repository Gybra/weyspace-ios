//
//  AppDelegate.m
//  WeySpace
//
//  Created by Luigi Leonardi on 13/02/13.
//  Copyright (c) 2013 Luigi Leonardi. All rights reserved.
//

#import "AppDelegate.h"

#import "FirstViewController.h"

#import "SecondViewController.h"

#import "ThirdViewController.h"

#import "ForthViewController.h"

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{    [[UIApplication sharedApplication]setStatusBarHidden:NO];

    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];

    UINavigationController *viewController1,*viewController2,*viewController3,*viewController4;
    if([[UIDevice currentDevice]userInterfaceIdiom] == UIUserInterfaceIdiomPhone){
        viewController1 = [[UINavigationController alloc]initWithRootViewController:[[FirstViewController alloc] initWithNibName:@"FirstViewController" bundle:nil]];
        viewController2 = [[UINavigationController alloc]initWithRootViewController:[[SecondViewController alloc] initWithNibName:@"SecondViewController" bundle:nil]];
        viewController3 = [[UINavigationController alloc]initWithRootViewController:[[ThirdViewController alloc] initWithNibName:@"ThirdViewController" bundle:nil]];
        viewController4 = [[UINavigationController alloc]initWithRootViewController:[[ForthViewController alloc]initWithNibName:@"ForthViewController" bundle:nil]];
    } else {
        viewController1 = [[UINavigationController alloc]initWithRootViewController:[[FirstViewController alloc] initWithNibName:@"FirstViewController_iPad" bundle:nil]];
        viewController2 = [[UINavigationController alloc]initWithRootViewController:[[SecondViewController alloc] initWithNibName:@"SecondViewController_iPad" bundle:nil]];
        viewController3 = [[UINavigationController alloc]initWithRootViewController:[[ThirdViewController alloc] initWithNibName:@"ThirdViewController_iPad" bundle:nil]];
        viewController4 = [[UINavigationController alloc]initWithRootViewController:[[ForthViewController alloc]initWithNibName:@"ForthViewController" bundle:nil]];
    }
    viewController1.navigationBar.tintColor = [UIColor blackColor];
    viewController2.navigationBar.tintColor = [UIColor blackColor];
    viewController3.navigationBar.tintColor = [UIColor blackColor];
    viewController4.navigationBar.tintColor = [UIColor blackColor];
    
    self.tabBarController = [[UITabBarController alloc] init];
    self.tabBarController.viewControllers = @[viewController1, viewController2,viewController3,viewController4];
    [[[self tabBarController] tabBar] setBackgroundImage:[UIImage imageNamed:@"tabbar.png"]];
    [[[self tabBarController] tabBar] setSelectionIndicatorImage:[UIImage imageNamed:@"tabbar_s.png"]];

    self.window.rootViewController = self.tabBarController;
    [self.window makeKeyAndVisible];
    [MKStoreManager sharedManager];
    sleep(1);
    return YES;
}

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    // attempt to extract a token from the url
    return [FBSession.activeSession handleOpenURL:url];
}


- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    [FBSession.activeSession handleDidBecomeActive];

}

- (void)applicationWillTerminate:(UIApplication *)application
{
    [FBSession.activeSession close];

    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

/*
// Optional UITabBarControllerDelegate method.
- (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController
{
}
*/

/*
// Optional UITabBarControllerDelegate method.
- (void)tabBarController:(UITabBarController *)tabBarController didEndCustomizingViewControllers:(NSArray *)viewControllers changed:(BOOL)changed
{
}
*/

@end
