//
//  Login.h
//  WeySpace
//
//  Created by Luigi Leonardi on 21/02/13.
//  Copyright (c) 2013 Luigi Leonardi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Login : UIViewController
@property (strong, nonatomic) IBOutlet UITextField *email;
@property (strong, nonatomic) IBOutlet UITextField *password;
- (IBAction)login;
@property (strong, nonatomic) IBOutlet UIButton *bottoncino;

@end
