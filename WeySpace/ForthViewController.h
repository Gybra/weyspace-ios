//
//  ForthViewController.h
//  WeySpace
//
//  Created by Luigi Leonardi on 27/02/13.
//  Copyright (c) 2013 Luigi Leonardi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MessageUI/MessageUI.h"
#import "InAppPacket.h"
#import "MobileCoreServices/MobileCoreServices.h"

@interface ForthViewController : UIViewController <MFMailComposeViewControllerDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIPopoverControllerDelegate>
- (IBAction)change:(id)sender;
- (IBAction)send:(id)sender;
-(IBAction)foto:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *bottoncino;
@property (strong, nonatomic) IBOutlet UIButton *bottoncino2;
@property (strong, nonatomic) IBOutlet UIButton *bottoncino3;

@end
