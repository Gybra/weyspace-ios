//
//  InAppPacket.m
//  WeySpace
//
//  Created by Luigi Leonardi on 27/02/13.
//  Copyright (c) 2013 Luigi Leonardi. All rights reserved.
//

#import "InAppPacket.h"
#import "MKStoreManager.h"
#import "AFHTTPClient.h"
#import "SBJson.h"

@interface InAppPacket ()

@end

@implementation InAppPacket

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self aggiorna];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)compra1:(id)sender {
    [self compra:@"com.weyspace.credits1"];
}

- (IBAction)compra2:(id)sender {
    [self compra:@"com.weyspace.credits2"];
}

- (IBAction)compra3:(id)sender {
    [self compra:@"com.weyspace.credits3"];
}

- (IBAction)compra4:(id)sender {
    [self compra:@"com.weyspace.credits4"];
}

- (IBAction)chiudi:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)compra:(NSString *)fea{
        
        [[MKStoreManager sharedManager] buyFeature:fea
                                        onComplete:^(NSString* purchasedFeature,
                                                     NSData* purchasedReceipt,
                                                     NSArray* availableDownloads)
         {
             int quanti = 0;
             
             if([purchasedFeature isEqual: @"com.weyspace.credits1"])
                 quanti = 4;
             else if([purchasedFeature isEqual: @"com.weyspace.credits2"])
                 quanti = 40;
             else if([purchasedFeature isEqual: @"com.weyspace.credits3"])
                 quanti = 80;
             else if([purchasedFeature isEqual: @"com.weyspace.credits4"])
                 quanti = 200;
             
             [self incrementa:quanti];
             [self manda:[NSString stringWithFormat:@"%i",quanti]];
             
         }
                                       onCancelled:^
         {
         }];
        
}

-(void)incrementa:(int)quanti{
  
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:@"http://95.110.196.250/weyspace/credits_used.php"]];
    
    NSDictionary *dictionary = [NSDictionary dictionaryWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults]valueForKey:@"credit"],@"credits",[[NSUserDefaults standardUserDefaults]valueForKey:@"email"],@"email",[NSString stringWithFormat:@"%i",quanti],@"bid", @"yes",@"inc", nil];
    
    [httpClient postPath:@"http://95.110.196.250/weyspace/credits_used.php" parameters:dictionary success:^(AFHTTPRequestOperation *operation, id responseObject){
        
        int ora = [[[NSUserDefaults standardUserDefaults]valueForKey:@"credit"]intValue];
        ora +=quanti;
        [[NSUserDefaults standardUserDefaults]setValue:[NSString stringWithFormat:@"%i",ora] forKey:@"credit"];
        [[NSUserDefaults standardUserDefaults]synchronize];
       
        [self aggiorna];
        

    }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"%@   error",error);
    }];
    
}

-(void)aggiorna{
        
        AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:@"http://95.110.196.250/weyspace/json.php"]];
        NSDictionary *dictionary = [NSDictionary dictionaryWithObjectsAndKeys:@"yes",@"used",[[NSUserDefaults standardUserDefaults]valueForKey:@"email"],@"email", nil];
        
        [httpClient postPath:@"http://95.110.196.250/weyspace/json.php" parameters:dictionary success:^(AFHTTPRequestOperation *operation, id responseObject){
            
            NSData *resp = (NSData *)responseObject;
            
            SBJsonParser *pars = [[SBJsonParser alloc]init];
            NSArray *rip = [pars objectWithData:resp];
            
            self.crediti.title = [NSString stringWithFormat:@"%@: %i",NSLocalizedString(@"crediti", nil),[[[rip objectAtIndex:0]objectForKey:@"Credits"]intValue]];
                        
            [[NSUserDefaults standardUserDefaults]setValue:[[rip objectAtIndex:0]objectForKey:@"Credits"] forKey:@"credit"];
            
            [[NSUserDefaults standardUserDefaults]synchronize];
            
        }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSLog(@"%@   error",error);
        }];
}

-(void)manda:(NSString *)how{

    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:@"http://95.110.196.250/weyspace/credits_bought.php"]];
    NSDictionary *dictionary = [NSDictionary dictionaryWithObjectsAndKeys:how,@"credits",[[NSUserDefaults standardUserDefaults]valueForKey:@"email"],@"email", nil];
    
    [httpClient postPath:@"http://95.110.196.250/weyspace/credits_bought.php" parameters:dictionary success:^(AFHTTPRequestOperation *operation, id responseObject){
        
    }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"%@   error",error);
    }];

}
@end
