//
//  InAppPacket.h
//  WeySpace
//
//  Created by Luigi Leonardi on 27/02/13.
//  Copyright (c) 2013 Luigi Leonardi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InAppPacket : UIViewController

- (IBAction)compra1:(id)sender;
- (IBAction)compra2:(id)sender;
- (IBAction)compra3:(id)sender;
- (IBAction)compra4:(id)sender;
- (IBAction)chiudi:(id)sender;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *crediti;

@end
