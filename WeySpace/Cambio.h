//
//  Cambio.h
//  WeySpace
//
//  Created by Luigi Leonardi on 28/02/13.
//  Copyright (c) 2013 Luigi Leonardi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Cambio : UIViewController
@property (strong, nonatomic) IBOutlet UITextField *email;
@property (strong, nonatomic) IBOutlet UITextField *password;
- (IBAction)cambia:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *bottoncino;

@end
