//
//  FirstViewController.h
//  WeySpace
//
//  Created by Luigi Leonardi on 13/02/13.
//  Copyright (c) 2013 Luigi Leonardi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ThirdViewController.h"
#import "MKStoreManager.h"

@interface FirstViewController : UIViewController <UITableViewDataSource,UITableViewDelegate,UIAlertViewDelegate,UIScrollViewDelegate>{

    NSMutableArray *dict,*dict_2,*dict3;
    BOOL pageControlBeingUsed;
    NSString *state;
    NSIndexPath *index;
    NSString *stato;


}
@property (strong, nonatomic) IBOutlet UIScrollView *scroll;
@property (strong, nonatomic) IBOutlet UIPageControl *page_control;
@property (nonatomic, retain) NSMutableArray *dict,*dict_2,*dict3;
@property (nonatomic, retain) NSString *state,*stato;
@property (nonatomic,retain) UITableView *tabella_1;
@property (nonatomic,retain) NSIndexPath *index;



- (IBAction)changePage;

@end
