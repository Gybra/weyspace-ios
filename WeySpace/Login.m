//
//  Login.m
//  WeySpace
//
//  Created by Luigi Leonardi on 21/02/13.
//  Copyright (c) 2013 Luigi Leonardi. All rights reserved.
//

#import "Login.h"
#import "SBJson.h"
#import "AFHTTPClient.h"

@interface Login ()

@end

@implementation Login

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
	[textField resignFirstResponder];
	return YES;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.view setBackgroundColor:[UIColor lightGrayColor]];
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 200, 40)];
    label.text = NSLocalizedString(@"logga", nil);
    label.center = self.bottoncino.center;
    label.backgroundColor = [UIColor clearColor];
    label.textColor = [UIColor whiteColor];
    label.font = [UIFont fontWithName:@"Helvetica-Bold" size:20];
    label.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:label];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)login {
       
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:@"http://95.110.196.250/weyspace/login.php"]];
    NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:self.email.text,@"email",self.password.text,@"password", nil];
    
    [httpClient postPath:@"http://95.110.196.250/weyspace/login.php" parameters:dict success:^(AFHTTPRequestOperation *operation, id responseObject){
        
        NSString *resp = (NSString *)responseObject;
        if([resp length]==0) {
            [[NSUserDefaults standardUserDefaults] setValue:self.email.text forKey:@"email"];
            [[NSUserDefaults standardUserDefaults] synchronize];

            [self dismissViewControllerAnimated:YES completion:nil];
        
        } else
            [[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"errore", nil) message:NSLocalizedString(@"login_fail", nil) delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil]show];
        
    }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"errore", nil) message:NSLocalizedString(@"login_fail", nil) delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil]show];

        NSLog(@"%@   error",error);
    }];
}


-(NSDictionary *)parseJsonFromUrl:(NSURL *)url
{
    SBJsonParser *parser = [[SBJsonParser alloc] init];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    NSData *response = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
    NSString *json_string = [[NSString alloc] initWithData:response encoding:NSUTF8StringEncoding];
    return [parser objectWithString:json_string];
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}
@end
