//
//  Lista.h
//  WeySpace
//
//  Created by Luigi Leonardi on 17/02/13.
//  Copyright (c) 2013 Luigi Leonardi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@interface Lista : UIViewController<UITableViewDataSource,UITableViewDelegate>
{

    NSMutableArray *dati;
    NSDictionary *famoso;

}

- (IBAction)chiudi:(id)sender;
@property (strong, nonatomic) IBOutlet UINavigationBar *navigation;
@property (strong, nonatomic) IBOutlet UITableView *tabella;
@property (nonatomic,retain) NSMutableArray *dati;
@property (nonatomic,retain) NSDictionary *famoso;

@end
