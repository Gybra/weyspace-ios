//
//  main.m
//  WeySpace
//
//  Created by Luigi Leonardi on 13/02/13.
//  Copyright (c) 2013 Luigi Leonardi. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
