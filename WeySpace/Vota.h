//
//  Vota.h
//  WeySpace
//
//  Created by Luigi Leonardi on 19/02/13.
//  Copyright (c) 2013 Luigi Leonardi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "InAppPacket.h"

@interface Vota : UIViewController<UIAlertViewDelegate,UIGestureRecognizerDelegate> {
    
    NSString *nome_testo,*id_utente,*url;
    
}

@property (strong, nonatomic) IBOutlet UILabel *nome;
@property (strong, nonatomic) IBOutlet UIImageView *foto;
@property (nonatomic, retain) NSString *nome_testo,*id_utente,*url;
@property (strong, nonatomic) IBOutlet UIImageView *immagine_utente;

- (IBAction)vota;
@property (strong, nonatomic) IBOutlet UIButton *bottoncino;

@end
