//
//  SecondViewController.m
//  WeySpace
//
//  Created by Luigi Leonardi on 13/02/13.
//  Copyright (c) 2013 Luigi Leonardi. All rights reserved.
//

#import "SecondViewController.h"
#import "SBJson.h"
#import "MLPCustomAutoCompleteCell.h"
#import "CoreLocation/CoreLocation.h"
#import "QuartzCore/QuartzCore.h"
#import "MHLazyTableImages.h"
#import "AFHTTPClient.h"
#import "Vota.h"


@interface SecondViewController () <FBLoginViewDelegate,MHLazyTableImagesDelegate>
@property (strong, nonatomic) id<FBGraphUser> loggedInUser;

@end

@implementation SecondViewController {
	MHLazyTableImages *_lazyImages;

}
@synthesize testo,image,users,finale,token,loggato,bottone,storico;
@synthesize loggedInUser = _loggedInUser;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = NSLocalizedString(@"voti", @"Votes");
        self.tabBarItem.image = [UIImage imageNamed:@"second"];
        _lazyImages = [[MHLazyTableImages alloc] init];
        _lazyImages.delegate = self;
        _lazyImages.placeholderImage = [UIImage imageNamed:@"Placeholder"];
        _lazyImages.tableView = self.tabella;
        
    }
    return self;
}
							
- (void)viewDidLoad

{
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:[NSString stringWithFormat:@"%@: %@",NSLocalizedString(@"crediti", nil),[[NSUserDefaults standardUserDefaults]valueForKey:@"credit"]] style:UIBarButtonItemStylePlain target:self action:@selector(compra)];;

    
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 200, 40)];
    label.text = NSLocalizedString(@"conferma", nil);
    label.center = self.bottoncino.center;
    label.backgroundColor = [UIColor clearColor];
    label.textColor = [UIColor whiteColor];
    label.font = [UIFont fontWithName:@"Helvetica-Bold" size:20];
    label.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:label];
    
    loggato=@"FALSE";
    [super viewDidLoad];
    self.tabella.layer.cornerRadius = 5;
    self.tabella.backgroundColor = [UIColor clearColor];
    
  NSString *plist_path=[[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:@"storico.plist"];
    
    NSString *root=[[NSBundle mainBundle]pathForResource:@"storico" ofType:@"plist"];
    
    if(![[NSFileManager defaultManager]fileExistsAtPath:plist_path])
        [[NSFileManager defaultManager]copyItemAtPath:root toPath:plist_path error:nil];
   
    storico=[[NSMutableArray alloc]initWithContentsOfFile:plist_path];
    
    FBLoginView *loginview = [[FBLoginView alloc] init];
    loginview.delegate = self;
    
    loginview.frame = CGRectOffset(loginview.frame, 5, 5);
if([[UIDevice currentDevice]userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
        loginview.center = CGPointMake(self.view.center.x, 183);
    else
        loginview.center = CGPointMake(self.view.center.x, 183+10);

    [self.view addSubview:loginview];
    
    [loginview sizeToFit];
    
    self.tabella.backgroundColor = [UIColor clearColor];
    self.tabella.backgroundView = nil;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)viewDidAppear:(BOOL)animated{
    [self aggiorna];
if(token ==NULL)
    [self prendiToken];
    
    NSString *plist_path=[[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:@"storico.plist"];
    storico=[[NSMutableArray alloc]initWithContentsOfFile:plist_path];
    
    [self.tabella reloadData];
    
}

- (IBAction)cerca:(id)sender {
    if([self.testo.text isEqual: @""])
        return;
    
    NSString *prendo = self.testo.text;
    [self.testo resignFirstResponder];
    NSString *urlString;
    if([loggato isEqual: @"TRUE"])
    {
        urlString = [NSString stringWithFormat:@"https://graph.facebook.com/search?q=%@&type=user&access_token=%@",[prendo stringByReplacingOccurrencesOfString:@" " withString:@"+"],self.token];
    
    
        users = [[self parseJsonFromUrl:[NSURL URLWithString:urlString]] objectForKey:@"data"];
        NSSortDescriptor *sort = [[NSSortDescriptor alloc]initWithKey:@"name" ascending:YES];
        [users sortUsingDescriptors:[NSArray arrayWithObject:sort]];

    }
    
    urlString = [NSString stringWithFormat:@"https://graph.facebook.com/search?q=%@&type=page",[prendo stringByReplacingOccurrencesOfString:@" " withString:@"+"]];
    
    NSMutableArray *pages = [[self parseJsonFromUrl:[NSURL URLWithString:urlString]]objectForKey:@"data"];
    
    Lista *lista = [[Lista alloc]initWithNibName:@"Lista" bundle:nil];
    lista.dati = users;
    if(pages.count>0)
    lista.famoso = [pages objectAtIndex:0];
    
    UINavigationController *control = [[UINavigationController alloc]initWithRootViewController:lista];
    
    control.navigationBar.tintColor = [UIColor blackColor];

    [self presentViewController:control animated:YES completion:nil];
}

-(UIImage *)getImage:(NSString *)chi{
    return [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?",chi]]]];
}

-(NSDictionary *)parseJsonFromUrl:(NSURL *)url
{
    SBJsonParser *parser = [[SBJsonParser alloc] init];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    NSData *response = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
    
    NSString *json_string = [[NSString alloc] initWithData:response encoding:NSUTF8StringEncoding];
    return [parser objectWithString:json_string];
}


-(void)prendiToken{
    self.token = [[FBSession activeSession]accessToken];
}

#pragma mark - FBLoginViewDelegate

- (void)loginViewShowingLoggedInUser:(FBLoginView *)loginView {
}

- (void)loginViewFetchedUserInfo:(FBLoginView *)loginView
                            user:(id<FBGraphUser>)user {
    loggato = @"TRUE";
    id_utente_loggato = user.id;
}

- (void)loginViewShowingLoggedOutUser:(FBLoginView *)loginView {
    loggato = @"FALSE";
    id_utente_loggato = NULL;

}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;

}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return [storico count];
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{

    return NSLocalizedString(@"votati", nil);

}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *cellindentifier = @"LazyTableCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellindentifier];
    if(cell==nil)
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellindentifier];
    cell.textLabel.text = [NSString stringWithFormat:@"%@",[[storico objectAtIndex:indexPath.row]objectForKey:@"name"]];

    [cell.textLabel setTextColor:[UIColor whiteColor]];
    
    cell.backgroundColor = [UIColor clearColor];
    cell.backgroundView = nil;

    [_lazyImages addLazyImageForCell:cell withIndexPath:indexPath];

    return cell;
    
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    Vota *vota;
    if([[UIDevice currentDevice]userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
        vota = [[Vota alloc]initWithNibName:@"Vota" bundle:nil];
    else
        vota = [[Vota alloc]initWithNibName:@"Vota_iPad" bundle:nil];
    
    vota.id_utente = [[self.storico objectAtIndex:indexPath.row]objectForKey:@"id"];
    vota.nome_testo = [[self.storico objectAtIndex:indexPath.row]objectForKey:@"name"];
    vota.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vota animated:YES];
    
}

- (NSURL *)lazyTableImages:(MHLazyTableImages *)lazyTableImages lazyImageURLForIndexPath:(NSIndexPath *)indexPath
{
    NSString *chi = [[storico objectAtIndex:indexPath.row]objectForKey:@"id"];
	return [NSURL URLWithString:[NSString stringWithFormat:@"http://graph.facebook.com/%@/picture",chi]];
}


- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
	[_lazyImages scrollViewDidEndDragging:scrollView willDecelerate:decelerate];
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
	[_lazyImages scrollViewDidEndDecelerating:scrollView];
}



-(void)compra{
    InAppPacket *pacchet;
    if([[UIDevice currentDevice]userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
        pacchet = [[InAppPacket alloc]initWithNibName:@"InAppPacket" bundle:nil];
    else
        pacchet = [[InAppPacket alloc]initWithNibName:@"InAppPacket_iPad" bundle:nil];
    
    [self presentViewController:pacchet animated:YES completion:nil];
}

-(void)incrementa{
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:@"http://95.110.196.250/weyspace/json.php"]];
    NSDictionary *dictionary = [NSDictionary dictionaryWithObjectsAndKeys:@"yes",@"credits", nil];
    
    [httpClient postPath:@"http://95.110.196.250/weyspace/json.php" parameters:dictionary success:^(AFHTTPRequestOperation *operation, id responseObject){
        
        NSData *resp = (NSData *)responseObject;
        
        SBJsonParser *pars = [[SBJsonParser alloc]init];
        NSArray *rip = [pars objectWithData:resp];
        int ora = [[[NSUserDefaults standardUserDefaults]valueForKey:@"credit"]intValue];
        ora += [[[rip objectAtIndex:0]objectForKey:@"How"]intValue];
        [[NSUserDefaults standardUserDefaults]setValue:[NSString stringWithFormat:@"%i",ora] forKey:@"credit"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        [self aggiorna];
        
        
    }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"%@   error",error);
    }];
    
    
    
}


-(void)aggiorna{
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:@"http://95.110.196.250/weyspace/json.php"]];
    NSDictionary *dictionary = [NSDictionary dictionaryWithObjectsAndKeys:@"yes",@"used",[[NSUserDefaults standardUserDefaults]valueForKey:@"email"],@"email", nil];
    
    [httpClient postPath:@"http://95.110.196.250/weyspace/json.php" parameters:dictionary success:^(AFHTTPRequestOperation *operation, id responseObject){
        
        NSData *resp = (NSData *)responseObject;
        
        SBJsonParser *pars = [[SBJsonParser alloc]init];
        NSArray *rip = [pars objectWithData:resp];
        
        self.navigationItem.rightBarButtonItem.title = [NSString stringWithFormat:@"%@: %i",NSLocalizedString(@"crediti", nil),[[[rip objectAtIndex:0]objectForKey:@"Credits"]intValue]];
        
        [[NSUserDefaults standardUserDefaults]setValue:[[rip objectAtIndex:0]objectForKey:@"Credits"] forKey:@"credit"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
    }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"%@   error",error);
    }];
    
}


- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 30)];
        [headerView setBackgroundColor:[UIColor clearColor]];
   
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(20, 3, tableView.bounds.size.width - 10, 18)];
    label.text = NSLocalizedString(@"votati", nil);
    label.font = [UIFont boldSystemFontOfSize:18.0f];
    label.textColor = [UIColor whiteColor];
    label.backgroundColor = [UIColor clearColor];
    [headerView addSubview:label];


    return headerView;
}
@end
