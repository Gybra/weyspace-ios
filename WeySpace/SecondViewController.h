//
//  SecondViewController.h
//  WeySpace
//
//  Created by Luigi Leonardi on 13/02/13.
//  Copyright (c) 2013 Luigi Leonardi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Lista.h"
#import <FacebookSDK/FacebookSDK.h>
#import "InAppPacket.h"

@interface SecondViewController : UIViewController <UITextFieldDelegate,NSURLConnectionDataDelegate,UITableViewDataSource,UITableViewDelegate>{

    NSMutableArray *users;
    NSMutableArray *finale;
    NSString *token;
    
    NSString *loggato;
    UIButton *bottone;
    NSMutableArray *storico;
    
}

- (IBAction)cerca:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *testo;
@property (strong, nonatomic) IBOutlet UIImageView *image;
@property (nonatomic, retain) NSMutableArray *users;
@property (nonatomic, retain) NSMutableArray *finale;
@property (nonatomic, retain) NSString *token,*loggato;
@property (nonatomic,retain) UIButton *bottone;
@property (nonatomic,retain) NSMutableArray *storico;
@property (strong, nonatomic) IBOutlet UITableView *tabella;
@property (strong, nonatomic) IBOutlet UIButton *bottoncino;



@end
