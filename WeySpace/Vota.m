//
//  Vota.m
//  WeySpace
//
//  Created by Luigi Leonardi on 19/02/13.
//  Copyright (c) 2013 Luigi Leonardi. All rights reserved.
//

#import "Vota.h"
#import "SBJson.h"
#import "MKStoreManager.h"
#import "AFHTTPClient.h"

@interface Vota ()

@end

@implementation Vota {

    UIAlertView *alert2,*alert3;
    UITapGestureRecognizer *uno,*due;

}
@synthesize nome_testo,id_utente,url,bottoncino;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 200, 40)];
    label.text = NSLocalizedString(@"vota", nil);
    label.center = self.bottoncino.center;
    label.backgroundColor = [UIColor clearColor];
    label.textColor = [UIColor whiteColor];
    label.font = [UIFont fontWithName:@"Helvetica-Bold" size:20];
    label.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:label];
    
    self.immagine_utente.contentMode = UIViewContentModeScaleAspectFit;
    
    if (([[UIScreen mainScreen]bounds].size.height != 568)&&([[UIDevice currentDevice]userInterfaceIdiom]==UIUserInterfaceIdiomPhone))
        bottoncino.center = CGPointMake(self.bottoncino.center.x, self.bottoncino.center.y-70);
    
	uno = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(mostra:)];
	[uno setNumberOfTapsRequired:1];
	[uno setDelegate:self];
    self.foto.userInteractionEnabled = YES;
	[self.foto addGestureRecognizer:uno];
    
    due = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(mostra:)];
	[due setNumberOfTapsRequired:1];
	[due setDelegate:self];
    self.immagine_utente.userInteractionEnabled = YES;
    [self.immagine_utente addGestureRecognizer:due];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:[NSString stringWithFormat:@"%@: 0",NSLocalizedString(@"crediti", nil)] style:UIBarButtonItemStylePlain target:self action:@selector(compra)];;

    [super viewDidLoad];
    self.nome.text = nome_testo;
    
    NSString *stringa;
    
    if ((!url)||(id_utente))
        stringa = [NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?width=200",id_utente];
    else
        stringa = self.url;
        
    UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:stringa]]];
    [self.foto setImage:image];
    self.foto.contentMode = UIViewContentModeScaleAspectFit;
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *plist = [documentsDirectory stringByAppendingPathComponent:@"con.plist"];
    
    NSMutableArray *array = [NSMutableArray arrayWithContentsOfFile:plist];
    if(array == NULL)
        array = [[NSMutableArray alloc]init];
    
    for(int i=0;i<array.count;i++)
        if([[array objectAtIndex:i]isEqualToString:self.id_utente])
            self.immagine_utente.image = [UIImage imageWithContentsOfFile:[documentsDirectory stringByAppendingPathComponent:@"profilo.png"]];
    

}

-(void)viewDidAppear:(BOOL)animated{

    [self aggiorna];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (IBAction)vota {
    alert2 = [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"attenzione", nil) message:[NSString stringWithFormat:@"%@ %@ ?",NSLocalizedString(@"quanti_voti", nil),nome_testo] delegate:self cancelButtonTitle:NSLocalizedString(@"annulla", nil) otherButtonTitles:@"Ok", nil];
    alert2.alertViewStyle = UIAlertViewStylePlainTextInput;
    [alert2 textFieldAtIndex:0].text =@"1";
    [alert2 show];
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
  if((alertView == alert2)&&(buttonIndex==1)){

        int quanti = [[[alertView textFieldAtIndex:0]text]intValue];
        [self eseguiVoto:self.id_utente nome:self.nome_testo quanti:quanti];
  } else if((alertView == alert3)&&(buttonIndex == 1))
        [self vaiSullaLuna:self.id_utente quanti:[[alertView textFieldAtIndex:0]text]];
  
}

-(void)eseguiVoto:(NSString*)numero nome:(NSString*)name quanti:(NSInteger)how {
    if(([[[NSUserDefaults standardUserDefaults]valueForKey:@"credit"]intValue]<=0)||([[[NSUserDefaults standardUserDefaults]valueForKey:@"credit"]intValue]<how)) {
        [[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"errore", nil) message:NSLocalizedString(@"non_bastano", nil) delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil]show];
        return;
        
    }
    
    NSString *email =[[NSUserDefaults standardUserDefaults]objectForKey:@"email"];
    
    NSString *pic = [NSString stringWithFormat:@"https://graph.facebook.com/%@/picture",numero];
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:@"http://95.110.196.250/weyspace/put_a_vote.php"]];
   
    NSDictionary *dizionario = [NSDictionary dictionaryWithObjectsAndKeys:name,@"nome",email,@"email",numero,@"udid",pic,@"pic",[NSString stringWithFormat:@"%i",how],@"credits", nil];
    [httpClient postPath:@"http://95.110.196.250/weyspace/put_a_vote.php" parameters:dizionario success:^(AFHTTPRequestOperation *operation, id responseObject){
                
        NSString *plist_path=[[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:@"storico.plist"];
        
        NSMutableArray *dict = [NSMutableArray arrayWithContentsOfFile:plist_path];
       
        if(dict == NULL)
            dict = [[NSMutableArray alloc]init];
        BOOL trovato = NO;
        for(int i=0;i<dict.count;i++)
            if([[[dict objectAtIndex:i]objectForKey:@"id"]isEqualToString:numero]){
                trovato = YES;
            }
        
        if(trovato== NO){
            
            NSDictionary *dict2 = [[NSDictionary alloc]initWithObjects:[NSArray arrayWithObjects:name,numero, nil] forKeys:[NSArray arrayWithObjects:@"name",@"id", nil]];
        [dict addObject:dict2];
        [dict writeToFile:plist_path atomically:YES];
        }
        
        [self decrementa:how];
        [self aggiorna];
        
        alert3 = [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"attenzione", nil) message:[NSString stringWithFormat:@"%@ %@ %@?",NSLocalizedString(@"vuoi_andare", nil),name,NSLocalizedString(@"quanti_crediti", nil)] delegate:self cancelButtonTitle:NSLocalizedString(@"annulla", nil) otherButtonTitles:@"Ok", nil];
        alert3.alertViewStyle = UIAlertViewStylePlainTextInput;
        [alert3 textFieldAtIndex:0].text =@"1";
        [alert3 show];
        
        
        
    }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"%@   error",error);
    }];
    
}

-(NSDictionary *)parseJsonFromUrl:(NSURL *)urlo
{
    SBJsonParser *parser = [[SBJsonParser alloc] init];
    NSURLRequest *request = [NSURLRequest requestWithURL:urlo];
    NSData *response = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
    
    NSString *json_string = [[NSString alloc] initWithData:response encoding:NSUTF8StringEncoding];
    return [parser objectWithString:json_string];
}

-(void)aggiorna{
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:@"http://95.110.196.250/weyspace/json.php"]];
    NSDictionary *dictionary = [NSDictionary dictionaryWithObjectsAndKeys:@"yes",@"used",[[NSUserDefaults standardUserDefaults]valueForKey:@"email"],@"email", nil];
    
    [httpClient postPath:@"http://95.110.196.250/weyspace/json.php" parameters:dictionary success:^(AFHTTPRequestOperation *operation, id responseObject){
        
        NSData *resp = (NSData *)responseObject;
        
        SBJsonParser *pars = [[SBJsonParser alloc]init];
        NSArray *rip = [pars objectWithData:resp];
        
        self.navigationItem.rightBarButtonItem.title = [NSString stringWithFormat:@"%@: %i",NSLocalizedString(@"crediti", nil),[[[rip objectAtIndex:0]objectForKey:@"Credits"]intValue]];
        
        [[NSUserDefaults standardUserDefaults]setValue:[[rip objectAtIndex:0]objectForKey:@"Credits"] forKey:@"credit"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
        
    }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"%@   error",error);
    }];
    
}

-(void)decrementa:(int)quanti{

    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:@"http://95.110.196.250/weyspace/credits_used.php"]];
    
    NSDictionary *dictionary = [NSDictionary dictionaryWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults]valueForKey:@"credit"],@"credits",[[NSUserDefaults standardUserDefaults]valueForKey:@"email"],@"email",[NSString stringWithFormat:@"%i",quanti],@"bid", @"yes",@"dec", nil];
    
    [httpClient postPath:@"http://95.110.196.250/weyspace/credits_used.php" parameters:dictionary success:^(AFHTTPRequestOperation *operation, id responseObject){
        
        int ora = [[[NSUserDefaults standardUserDefaults]valueForKey:@"credit"]intValue];
        ora -=quanti;
        [[NSUserDefaults standardUserDefaults]setValue:[NSString stringWithFormat:@"%i",ora] forKey:@"credit"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        [self aggiorna];
        
        
    }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"%@   error",error);
    }];

}

-(void)compra{
    InAppPacket *pacchet = [[InAppPacket alloc]initWithNibName:@"InAppPacket" bundle:nil];
    [self presentViewController:pacchet animated:YES completion:nil];
}

-(void)incrementa{
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:@"http://95.110.196.250/weyspace/json.php"]];
    NSDictionary *dictionary = [NSDictionary dictionaryWithObjectsAndKeys:@"yes",@"credits", nil];
    
    [httpClient postPath:@"http://95.110.196.250/weyspace/json.php" parameters:dictionary success:^(AFHTTPRequestOperation *operation, id responseObject){
        
        NSData *resp = (NSData *)responseObject;
        
        SBJsonParser *pars = [[SBJsonParser alloc]init];
        NSArray *rip = [pars objectWithData:resp];
        int ora = [[[NSUserDefaults standardUserDefaults]valueForKey:@"credit"]intValue];
        ora += [[[rip objectAtIndex:0]objectForKey:@"How"]intValue];
        
        [[NSUserDefaults standardUserDefaults]setValue:[NSString stringWithFormat:@"%i",ora] forKey:@"credit"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        [self aggiorna];
        
        
    }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"%@   error",error);
    }];
    
    
    
}

-(void)vaiSullaLuna:(NSString *)udid quanti:(NSString *)how{

    
    if(([[[NSUserDefaults standardUserDefaults]valueForKey:@"credit"]intValue]<=0)||
       ([[[NSUserDefaults standardUserDefaults]valueForKey:@"credit"]intValue]<[how intValue]))
    {
        [[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"errore", nil) message:NSLocalizedString(@"non_bastano", nil) delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil]show];
        return;
        
    }
    
    if([how intValue]==0)
        return;
    
    NSString *email =[[NSUserDefaults standardUserDefaults]objectForKey:@"email"];
        
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:@"http://95.110.196.250/weyspace/go_with.php"]];
    
    NSDictionary *dizionario = [NSDictionary dictionaryWithObjectsAndKeys:udid,@"udid",email,@"email",how,@"credits", nil];
    
    [httpClient postPath:@"http://95.110.196.250/weyspace/go_with.php" parameters:dizionario success:^(AFHTTPRequestOperation *operation, id responseObject){
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString *imagePath = [documentsDirectory stringByAppendingPathComponent:@"profilo.png"];
 
        if([[NSFileManager defaultManager]fileExistsAtPath:imagePath]){
            UIImage *image = [UIImage imageWithContentsOfFile:imagePath];
            self.immagine_utente.image = image;

        }else if(id_utente_loggato){
            UIImage *img = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?width=300",id_utente_loggato]]]];
            self.immagine_utente.image = img;
            [self salvaFoto:img];
            
        } else
            [[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"errore", nil) message:NSLocalizedString(@"no_foto", nil) delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil]show];
        
        NSString *plist_vai_con = [documentsDirectory stringByAppendingPathComponent:@"con.plist"];
        if(![[NSFileManager defaultManager]fileExistsAtPath:plist_vai_con]){
            NSString *perc = [[NSBundle mainBundle]pathForResource:@"storico" ofType:@"plist"];
            [[NSFileManager defaultManager]copyItemAtPath:perc toPath:plist_vai_con error:nil];
        }
        
        NSMutableArray *letto = [NSMutableArray arrayWithContentsOfFile:plist_vai_con];
        if (letto==NULL)
            letto = [[NSMutableArray alloc]init];
        
        [letto addObject:udid];
        [letto writeToFile:plist_vai_con atomically:YES];
        [self decrementa:[how intValue]];
        [self aggiorna];
        
    }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"%@   error",error);
    }];
}

-(void)mostra: (UITapGestureRecognizer *)recog{
    
    if((recog == due)&&(self.immagine_utente.image == NULL))
        return;
    
    NSString *stringa;
  
    if((!url)||(id_utente))
        stringa = [NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?width=400",id_utente];
    else
        stringa = self.url;
    
    
    UIView *vistona = [[UIView alloc]initWithFrame:[[UIScreen mainScreen]bounds]];
    vistona.backgroundColor = [UIColor blackColor];
    
    UIViewController *co = [[UIViewController alloc]init];
    co.view = vistona;
    
    UIImageView *immagine = [[UIImageView alloc]init];
    immagine.frame = CGRectMake(0, 0, 400, vistona.frame.size.width);

    if(recog == due)
        immagine.image = self.immagine_utente.image;
    else{
        immagine.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:stringa]]];

        if(!url)
            immagine.frame = CGRectMake(0, 0, immagine.image.size.width, immagine.image.size.height);
        }
    immagine.contentMode = UIViewContentModeScaleAspectFit;

    [vistona addSubview:immagine];
    UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:co];
    co.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:NSLocalizedString(@"fatto", nil) style:UIBarButtonItemStyleDone target:self action:@selector(chiudi)];
    co.title = nome_testo;
    
    immagine.center = CGPointMake(vistona.center.x, vistona.center.y-nav.navigationBar.frame.size.height);

    nav.navigationBar.tintColor = [UIColor blackColor];
    [self presentViewController:nav animated:YES completion:nil];
}

-(void)chiudi{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)salvaFoto:(UIImage *)img{

    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *imagePath = [documentsDirectory stringByAppendingPathComponent:@"profilo.png"];
        
    NSData *webData = UIImageJPEGRepresentation(img, 1);
    [webData writeToFile:imagePath atomically:YES];   
    

}

- (void)viewDidUnload {
    [self setBottoncino:nil];
    [super viewDidUnload];
}
@end
